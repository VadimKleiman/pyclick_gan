# План на ближайшую 1-2 недели
* Детальное изучение библиотеки tensorflow и lstm из библиотеки pyclick
* Понять как переиспользовать существующий код (дописать/переписать) для того чтобы добавить в GAN как generator
* Поискать рекомендации (может статьи) по архитектуре для классификатора. (P.S. статей пока не нашел)
* Реализовать GAN
* Прогнать часть данных для обучения GAN и посчитать метрики, чтобы оценить масштаб проблем :)
# Дальнейший план
* Обучить на полных данных lstm GAN и некоторые из стандартных кликовых моделей (если это нужно) и посчитать метрики
* Реализовать некоторые модификации GAN (CGAN и т.п.). Может получиться лучше чем обычные GAN-ы.
* Написать текст для диплома
# Литература
[Много статей про GAN-ы и как их обучать](https://deeplearning4j.org/generative-adversarial-network)

[Improved Techniques for Training GANs](https://arxiv.org/pdf/1606.03498.pdf)

[GAN оригинальная статья](https://arxiv.org/pdf/1406.2661v1.pdf)

[DeLiGAN](https://arxiv.org/abs/1706.02071)
