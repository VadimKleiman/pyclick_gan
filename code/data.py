import sys
import numpy as np
sys.path.insert(0, 'utils')
from config import *
from batches import Batches
from sklearn.model_selection import train_test_split


class Data:
    def __init__(self, data_dir=DATA_DIR, file_name=TEST_FILE, size=5):
        self.data_dir = data_dir
        self.file_name = file_name
        self.size = size
        self.batches = Batches(data_dir + file_name, size)
        self.q0 = np.zeros((size, 10, 1024))
        self.d0 = np.zeros((size, 1, 20480))
        self.l0 = np.zeros((size, 1, 1))

    def get_train_test(self, test_size=0.1):
        _, _, batch_queries, batch_docs, batch_seq_labels, _ = self.batches.create_batch()
        return train_test_split(self.__build_data(batch_queries,
                                                  batch_docs,
                                                  batch_seq_labels).reshape((self.size, 236555)),
                                np.array([1] * self.size),
                                test_size=test_size,
                                random_state=42)

    def get_data(self):
        _, _, batch_queries, batch_docs, batch_seq_labels, _ = self.batches.create_batch()
        return self.__build_data(batch_queries, batch_docs, batch_seq_labels).reshape((self.size, 236555)), np.array([1] * self.size)

    def get_data_without_clicks(self):
        _, _, batch_queries, batch_docs, batch_seq_labels, _ = self.batches.create_batch()
        return self.__build_data_without_clicks(batch_queries, batch_docs).reshape((self.size, 236544)), batch_seq_labels

    def __build_data_without_clicks(self, q, doc):
        q = np.expand_dims(q, 1)
        q2 = np.concatenate((q, self.q0), 1)
        d = np.concatenate((self.d0, doc), 1)
        data = np.concatenate((q2, d), 2)
        return data

    def __build_data(self, q, doc, label):
        q = np.expand_dims(q, 1)
        q2 = np.concatenate((q, self.q0), 1)
        d = np.concatenate((self.d0, doc), 1)
        label = np.expand_dims(label, 1)
        label = np.reshape(label, (self.size, 10, 1))
        l2 = np.concatenate((self.l0, label), 1)
        data = np.concatenate((q2, l2, d), 2)
        return data


class SimulateData(Data):

    def __init__(self, data_dir=DATA_DIR, file_name=TEST_FILE, size=5):
        Data.__init__(self, data_dir, file_name, size)

    def get_train_test(self, test_size=0.01):
        _, _, batch_queries, batch_docs, batch_seq_labels, _ = self.batches.create_batch()
        return train_test_split(self.__build_data(batch_queries,
                                                  batch_docs,
                                                  np.random.randint(2, size=(self.size, 10))).reshape((self.size, 236555)),
                                np.array([0] * self.size),
                                test_size=test_size,
                                random_state=42)

    def get_data_without_clicks(self):
        _, _, batch_queries, batch_docs, _, _ = self.batches.create_batch()
        return self.__build_data_without_clicks(batch_queries, batch_docs).reshape((self.size, 236544)), np.random.randint(2, size=(self.size, 10))

    def get_data(self):
        _, _, batch_queries, batch_docs, batch_seq_labels, _ = self.batches.create_batch()
        return self.__build_data(batch_queries, batch_docs, np.random.randint(2, size=(self.size, 10))).reshape((self.size, 236555)), np.array([0] * self.size)

    def __build_data(self, q, doc, label):
        q = np.expand_dims(q, 1)
        q2 = np.concatenate((q, self.q0), 1)
        d = np.concatenate((self.d0, doc), 1)
        label = np.expand_dims(label, 1)
        label = np.reshape(label, (self.size, 10, 1))
        l2 = np.concatenate((self.l0, label), 1)
        data = np.concatenate((q2, l2, d), 2)
        return data