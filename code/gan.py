# coding=utf-8
import os
import sys
import pydot
sys.path.insert(0, 'utils')
from batches import Batches
from config import *
from keras.utils import plot_model
from keras.layers import Activation, Input, Dropout, Dense, Reshape, Flatten, concatenate, Conv2D, MaxPooling2D, RepeatVector
from keras.layers import BatchNormalization
from keras.layers.advanced_activations import LeakyReLU
from keras.models import Sequential, Model
from keras.optimizers import Adam
import numpy as np
from keras.models import Model
from data import Data
import scipy.stats
import matplotlib.pyplot as plt


class GAN_clicks:
    def __init__(self, gan_type='gan_stride'):
        self.gen_input = (11, 21504, 1)
        self.discr_input = (11, 21514, 1)
        self.positions_of_clicks = np.zeros(10)
        self.clicks_per_sessions = np.zeros(11)
        self.g = None
        self.d = None
        self.gan_type = gan_type

    def build_generator(self):
        if self.gan_type == 'gan_stride':
            model = Sequential()
            model.add(Conv2D(4, (5, 5), padding='same', input_shape=self.gen_input))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            # model.add(MaxPooling2D(pool_size=(1, 4)))
            model.add(Conv2D(4, (5, 5), strides=2, padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            # model.add(MaxPooling2D(pool_size=(2, 4)))
            model.add(Conv2D(4, (5, 5), strides=4, padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            # model.add(MaxPooling2D(pool_size=(2, 4)))
            model.add(Conv2D(4, (5, 5), strides=8, padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            model.add(Conv2D(1, (5, 5), strides=16, padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            # model.add(MaxPooling2D(pool_size=(2, 32)))
            model.add(Flatten())
            model.add(Dense(10))
            model.add(Activation('sigmoid'))
            model.summary()
            input_gen = Input(shape=self.gen_input, name='input_gen')
            clicks = model(input_gen)
            return Model(input_gen, clicks)
        else:
            model = Sequential()
            model.add(Conv2D(8, (5, 5), padding='same', input_shape=self.gen_input))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            model.add(MaxPooling2D(pool_size=(1, 4)))
            model.add(Conv2D(8, (5, 5), padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            model.add(MaxPooling2D(pool_size=(2, 4)))
            model.add(Conv2D(8, (5, 5), padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            model.add(MaxPooling2D(pool_size=(2, 4)))
            model.add(Conv2D(1, (5, 5), padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            model.add(MaxPooling2D(pool_size=(2, 32)))
            model.add(Flatten())
            model.add(Activation('sigmoid'))
            input_gen = Input(shape=self.gen_input, name='input_gen')
            clicks = model(input_gen)
            return Model(input_gen, clicks)

    def build_discriminator(self):
        if self.gan_type == 'gan_stride':
            model = Sequential()
            model.add(Conv2D(8, (5, 5), padding='same', input_shape=self.discr_input))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            model.add(Dropout(0.25))
            # model.add(MaxPooling2D(pool_size=(1, 4)))
            model.add(Conv2D(16, (5, 5), strides=2, padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            model.add(Dropout(0.25))
            # model.add(MaxPooling2D(pool_size=(2, 4)))
            model.add(Conv2D(32, (5, 5), strides=4, padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            model.add(Dropout(0.25))
            # model.add(MaxPooling2D(pool_size=(2, 4)))
            model.add(Conv2D(16, (5, 5), strides=8, padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            model.add(Dropout(0.25))
            model.add(Conv2D(1, (5, 5), strides=16, padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(BatchNormalization(momentum=0.8))
            model.add(Dropout(0.25))
            # model.add(MaxPooling2D(pool_size=(2, 16)))
            model.add(Flatten())
            model.add(Dense(1, activation='sigmoid'))
            input_discr = Input(shape=self.discr_input, name='input_discr')
            prob = model(input_discr)
            model.summary()
            return Model(input_discr, prob)
        else:
            model = Sequential()
            model.add(Conv2D(8, (5, 5), padding='same', input_shape=self.discr_input))
            model.add(LeakyReLU(alpha=0.2))
            model.add(Dropout(0.25))
            model.add(MaxPooling2D(pool_size=(1, 4)))
            model.add(Conv2D(8, (5, 5), padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(Dropout(0.25))
            model.add(MaxPooling2D(pool_size=(2, 4)))
            model.add(Conv2D(8, (5, 5), padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(Dropout(0.25))
            model.add(MaxPooling2D(pool_size=(2, 4)))
            model.add(Conv2D(1, (5, 5), padding='same'))
            model.add(LeakyReLU(alpha=0.2))
            model.add(Dropout(0.25))
            model.add(MaxPooling2D(pool_size=(2, 16)))
            model.add(Flatten())
            model.add(Dense(1, activation='sigmoid'))
            input_discr = Input(shape=self.discr_input, name='input_discr')
            prob = model(input_discr)
            return Model(input_discr, prob)

    def generator_containing_discriminator(self, generator, discriminator):
        input_gen = Input(shape=self.gen_input, name='input_gen')
        x_generator = generator(input_gen)
        repeat = RepeatVector(11)(x_generator)
        x = Reshape((11, 10, 1))(repeat)
        merged = concatenate([input_gen, x], axis=2)
        discriminator.trainable = False
        x_discriminator = discriminator(merged)
        model = Model(inputs=input_gen, outputs=[x_generator, x_discriminator])
        return model

    def train(self):
        discriminator = self.build_discriminator()
        discriminator.summary()
        generator = self.build_generator()
        generator.summary()
        discriminator_on_generator = self.generator_containing_discriminator(generator, discriminator)
        d_optim = Adam(0.0002, 0.5)
        g_optim = Adam(0.0002, 0.5)
        generator.compile(loss='binary_crossentropy', optimizer=g_optim)
        discriminator_on_generator.compile(loss='binary_crossentropy', optimizer='adam')
        discriminator.trainable = True
        discriminator.compile(loss='binary_crossentropy', optimizer=d_optim)
        d = Data('/Users/vadim/', 'test_query_sessions_with_behavioral_features.part_0', 32)
        for epoch in range(1):
            print("Epoch is", epoch)
            for index in xrange(1000000 / 32):
                X_real, y_real = d.get_data_without_clicks()
                y_tmp = y_real
                X_real = X_real.reshape((32, 11, 21504, 1))
                y_real = y_real.reshape((32, 10, 1))
                y_real = np.expand_dims(y_real, axis=1)
                y_real = np.repeat(y_real, 11, axis=1)
                generate_clicks = generator.predict(X_real)
                for i in xrange(0, generate_clicks.shape[0]):
                    for j in xrange(0, generate_clicks.shape[1]):
                        generate_clicks[i][j] = scipy.stats.bernoulli.rvs(generate_clicks[i][j], size=1)[0]
                generate_clicks = generate_clicks.reshape((32, 10, 1))
                generate_clicks = np.expand_dims(generate_clicks, axis=1)
                generate_clicks = np.repeat(generate_clicks, 11, axis=1)
                X_real_qsc = np.concatenate([X_real, y_real], axis=2)
                X_fake_qsc = np.concatenate([X_real, generate_clicks], axis=2)
                X = np.concatenate([X_real_qsc, X_fake_qsc])
                y = np.concatenate([np.ones(shape=(32, 1)), np.zeros(shape=(32, 1))])
                d_loss = discriminator.train_on_batch(X, y)
                pred_temp = discriminator.predict(X)
                print("batch %d d_loss : %f" % (index, d_loss))
                discriminator.trainable = False
                g_loss = discriminator_on_generator.train_on_batch(X_real, [y_tmp, np.ones((32, 1))])
                discriminator.trainable = True
                print("batch %d g_loss : %f" % (index, g_loss[1]))
                if index % 20 == 0:
                    generator.save_weights('generator', True)
                    discriminator.save_weights('discriminator', True)

    def get_stat_clicks(self, batch_size=32, path_dir='/Users/vadim/', file_name='test_query_sessions_with_behavioral_features.part_3'):
        generator = self.build_generator()
        generator.load_weights('generator')
        d = Data(path_dir, file_name, batch_size)
        for index in xrange(500000 / batch_size):
            print str(index) + '/' + str(500000 // batch_size)
            X_real, y_real = d.get_data_without_clicks()
            X_real = X_real.reshape((batch_size, 11, 21504, 1))
            generate_clicks = generator.predict(X_real)
            for i in xrange(0, generate_clicks.shape[0]):
                for j in xrange(0, generate_clicks.shape[1]):
                    generate_clicks[i][j] = scipy.stats.bernoulli.rvs(generate_clicks[i][j], size=1)[0]
            self.positions_of_clicks = np.sum([self.positions_of_clicks, np.sum([generate_clicks], axis=1)], axis=0)
            for pos in np.sum([generate_clicks], axis=2)[0]:
                self.clicks_per_sessions[int(pos)] += 1
        self.positions_of_clicks /= np.sum([self.positions_of_clicks])
        self.positions_of_clicks = self.positions_of_clicks[0]
        self.clicks_per_sessions /= np.sum([self.clicks_per_sessions])
        return self.positions_of_clicks, self.clicks_per_sessions

    def get_clicks(self, X_real, batch_size=32):
        X_real = X_real.reshape((batch_size, 11, 21504, 1))
        generate_clicks = self.g.predict(X_real)
        return generate_clicks

    def load(self, path):
        self.g = self.build_generator()
        if self.gan_type == 'gan_stride':
            self.g.load_weights(path+'generator')
        else:
            self.g.load_weights(path + 'simple_generator')

    def view(self):
        discriminator = self.build_discriminator()
        generator = self.build_generator()
        discriminator_on_generator = self.generator_containing_discriminator(generator, discriminator)
        plot_model(discriminator_on_generator, to_file='gan.png', show_shapes=True, show_layer_names=False)

if __name__ == '__main__':
    gan = GAN_clicks()
    gan.train()
    # gan.view()
    # poc, cps = gan.get_stat_clicks()
    # people = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    # x_pos = np.arange(len(people))
    # plt.bar(x_pos, poc, align='center')
    # plt.xticks(x_pos, people)
    # plt.ylabel('Проценты'.decode('utf-8'))
    # plt.xlabel('Позиции кликов'.decode('utf-8'))
    # plt.show()
    # people = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    # x_pos = np.arange(len(people))
    # plt.bar(x_pos, cps, align='center')
    # plt.xticks(x_pos, people)
    # plt.ylabel('Проценты'.decode('utf-8'))
    # plt.xlabel('Количество кликов'.decode('utf-8'))
    # plt.show()