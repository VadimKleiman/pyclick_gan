# coding=utf-8
from gan import GAN_clicks
from iter_classifiers import PartialClassifiers
from sklearn.metrics import accuracy_score
from data import Data
import numpy as np
from sklearn.metrics import confusion_matrix
from pathlib import Path
import cPickle
import scipy.stats
import math
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

class Test:
    def start_test(self):
        raise NotImplementedError


class TestFramework(Test):
    def __init__(self,
                 type_model,
                 path_to_save_gan,
                 path_to_cls,
                 data_dir,
                 file_names,
                 path_to_output,
                 batch_size,
                 count_session):
        print 'MODEL_TYPE:\t' + type_model
        print 'DATA_DIR:\t' + data_dir
        print 'FILES:\t' + str(file_names)
        print 'CLS_DIR:\t' + path_to_cls
        print 'GAN_DIR:\t' + path_to_save_gan
        print 'OUTPUT:\t' + path_to_output
        print 'BATCH_SIZE:\t' + str(batch_size)
        print 'COUNT_SESSION:\t' + str(count_session)
        if type_model == 'gan' or type_model == 'gan_stride':
            self.gan = GAN_clicks(type_model)
            print 'LOAD_GAN'
            self.gan.load(path_to_save_gan)
        self.metrics = {
            'TP': None,
            'FP': None,
            'FN': None,
            'TN': None,
            'SCORE': None,
            'LL': 0.0,
            'PR': 0.0
        }
        self.gan_clicks = []
        self.path_to_save_gan = path_to_save_gan
        self.path_to_cls = path_to_cls
        self.data_dir = data_dir
        self.file_names = file_names
        self.path_to_output = path_to_output
        self.batch_size = batch_size
        self.count_session = count_session
        self.predict = None
        self.target = None
        self.error_files = 0
        self.type_model = type_model
        self.lstm = open('/Users/vadim/clicks', 'r')

    def start_test(self):
        pc = PartialClassifiers()
        print 'LOAD CLS'
        pc.load(self.path_to_cls)
        all_real_clicks = None
        all_fake_clicks = None
        for file_name in self.file_names:
            print 'Current file:' + file_name
            current_file = Path(self.data_dir + file_name)
            if current_file.exists() and current_file.is_file():
                batch = Data(self.data_dir, file_name, self.batch_size)
                for it in xrange(0, 20):
                    for id_batch in xrange(0, self.count_session // self.batch_size):
                        print 'Iter: ' + str(it) + ' Current batch:\t' + str(id_batch) + '/' + str(self.count_session // self.batch_size)
                        data, clicks = batch.get_data_without_clicks()
                        fake_clicks = self.__get_clicks(data)
                        if all_fake_clicks is None:
                            all_fake_clicks = fake_clicks
                            all_real_clicks = clicks
                        else:
                            all_fake_clicks = np.concatenate([all_fake_clicks, fake_clicks])
                            all_real_clicks = np.concatenate([all_real_clicks, clicks])
                        y_fake = np.zeros((self.batch_size,))
                        for index in xrange(0, self.batch_size):
                            if np.array_equal(clicks[index], fake_clicks[index]):
                                y_fake[index] = 1
                        data = data.reshape(self.batch_size, 11, 21504)
                        arr = np.split(data, [1024], axis=2)
                        l0 = np.zeros((self.batch_size, 1, 1))
                        fake_clicks = np.expand_dims(fake_clicks, 1)
                        fake_clicks = np.reshape(fake_clicks, (self.batch_size, 10, 1))
                        fake_clicks = np.concatenate([l0, fake_clicks], axis=1)
                        X_fake = np.concatenate([arr[0], fake_clicks, arr[1]], axis=2).reshape(self.batch_size, 236555)
                        if self.predict is None:
                            self.predict = pc.predict(X_fake)
                            self.target = y_fake
                            continue
                        tmp_predict = pc.predict(X_fake)
                        for index in xrange(0, len(tmp_predict)):
                            self.predict[index] = np.concatenate([self.predict[index], tmp_predict[index]])
                        self.target = np.concatenate([self.target, y_fake])

                    # self.metrics['LL'] = {"ALL": self.__likelihood(all_fake_clicks, all_real_clicks)}
                    for index, name in enumerate(pc.get_name_cls()):
                        tn, fp, fn, tp = confusion_matrix(self.target, self.predict[index]).ravel()
                        if self.metrics['TP'] is None:
                            self.metrics['TP'] = {name: tp}
                            self.metrics['FN'] = {name: fn}
                            self.metrics['TN'] = {name: tn}
                            self.metrics['FP'] = {name: fp}
                            self.metrics['SCORE'] = {name: accuracy_score(self.target, self.predict[index])}
                        #         # print self.__likelihood(all_fake_clicks, all_real_clicks)
                        #         self.metrics['LL'] = {name: self.__likelihood(all_fake_clicks, all_real_clicks)}
                        else:
                            self.metrics['TP'][name] = tp
                            self.metrics['FN'][name] = fn
                            self.metrics['TN'][name] = tn
                            self.metrics['FP'][name] = fp
                            self.metrics['SCORE'][name] = accuracy_score(self.target, self.predict[index])
                    #         self.metrics['LL'] = {name: self.__likelihood(all_fake_clicks, all_real_clicks)}
                    f_mae = 0
                    l_mae = 0
                    for i in xrange(0, all_real_clicks.shape[0]):
                        t_first = 0
                        t_second = 0
                        r_first = 0
                        r_second = 0
                        t = np.where(all_real_clicks[i] == 1)[0]
                        r = np.where(all_fake_clicks[i] == 1)[0]
                        if t.shape[0] != 0:
                            t_first = t[0] + 1
                            t_second = t[t.shape[0] - 1] + 1
                        if r.shape[0] != 0:
                            r_first = r[0] + 1
                            r_second = r[r.shape[0] - 1] + 1
                        f_mae += math.fabs(t_first - r_first)
                        l_mae += math.fabs(t_second - r_second)
                    print 'F MAE' + str(f_mae / all_real_clicks.shape[0])
                    print 'L MAE' + str(l_mae / all_real_clicks.shape[0])
                    print self.get_result()
                    self.metrics = {
                        'TP': None,
                        'FP': None,
                        'FN': None,
                        'TN': None,
                        'SCORE': None,
                        'LL': 0.0,
                        'PR': 0.0
                    }
                    all_real_clicks = None
                    all_fake_clicks = None
                    self.target = None
                    self.predict = None
            else:
                print 'ERROR: ' + self.data_dir + file_name
                self.error_files += 1
        # self.metrics['LL'] = {"ALL": self.__likelihood(all_fake_clicks, all_real_clicks)}
        for index, name in enumerate(pc.get_name_cls()):
            tn, fp, fn, tp = confusion_matrix(self.target, self.predict[index]).ravel()
            if self.metrics['TP'] is None:
                self.metrics['TP'] = {name: tp}
                self.metrics['FN'] = {name: fn}
                self.metrics['TN'] = {name: tn}
                self.metrics['FP'] = {name: fp}
                self.metrics['SCORE'] = {name: accuracy_score(self.target, self.predict[index])}
        #         # print self.__likelihood(all_fake_clicks, all_real_clicks)
        #         self.metrics['LL'] = {name: self.__likelihood(all_fake_clicks, all_real_clicks)}
            else:
                self.metrics['TP'][name] = tp
                self.metrics['FN'][name] = fn
                self.metrics['TN'][name] = tn
                self.metrics['FP'][name] = fp
                self.metrics['SCORE'][name] = accuracy_score(self.target, self.predict[index])
        #         self.metrics['LL'] = {name: self.__likelihood(all_fake_clicks, all_real_clicks)}
        f_mae = 0
        l_mae = 0
        for i in xrange(0, all_real_clicks.shape[0]):
            t_first = 0
            t_second = 0
            r_first = 0
            r_second = 0
            t = np.where(all_real_clicks[i] == 1)[0]
            r = np.where(all_fake_clicks[i] == 1)[0]
            if t.shape[0] != 0:
                t_first = t[0] + 1
                t_second = t[t.shape[0] - 1] + 1
            if r.shape[0] != 0:
                r_first = r[0] + 1
                r_second = r[r.shape[0] - 1] + 1
            f_mae += math.fabs(t_first - r_first)
            l_mae += math.fabs(t_second - r_second)
        print 'F MAE' + str(f_mae / all_real_clicks.shape[0])
        print 'L MAE' + str(l_mae / all_real_clicks.shape[0])
        print 'SAVE METRICS'
        with open(self.path_to_output + self.type_model + '_metrics.pkl', 'wb') as fid:
            cPickle.dump(self.metrics, fid)

    def __get_clicks(self, data):
        if self.type_model == 'gan' or self.type_model == 'gan_stride':
            generate_clicks = self.gan.get_clicks(data, self.batch_size)
            for i in xrange(0, generate_clicks.shape[0]):
                for j in xrange(0, generate_clicks.shape[1]):
                    generate_clicks[i][j] = scipy.stats.bernoulli.rvs(generate_clicks[i][j], size=1)[0]
            return generate_clicks
        elif self.type_model == 'zeros':
            return np.zeros((self.batch_size, 10))
        elif self.type_model == 'ones':
            result = np.zeros((self.batch_size, 10))
            for j in xrange(0, self.batch_size):
                result[j][0] = 1
            return result
        elif self.type_model == 'lstm':
            generate_clicks = None
            for i in xrange(0, self.batch_size):
                t = np.array(self.lstm.readline().split(' '), dtype='float32').reshape(1, 10)
                if generate_clicks is None:
                    generate_clicks = t
                else:
                    generate_clicks = np.concatenate([generate_clicks, t])
            for i in xrange(0, generate_clicks.shape[0]):
                for j in xrange(0, generate_clicks.shape[1]):
                    generate_clicks[i][j] = scipy.stats.bernoulli.rvs(generate_clicks[i][j], size=1)[0]
            return generate_clicks


    def get_result(self):
        return self.metrics

    def get_total_session_count(self):
        c = (self.count_session // self.batch_size) * self.batch_size
        return c * len(self.file_names) - self.error_files * c

    def __likelihood(self, predictions, labels):
        loglikelihood = 0.0
        for i, session in enumerate(predictions):
            click_probs = self.__get_conditional_click_probs(session, labels[i])
            log_click_probs = [math.log(prob) for prob in click_probs]
            loglikelihood += sum(log_click_probs) / len(log_click_probs)
        loglikelihood /= predictions.shape[0]
        return loglikelihood

    def __get_conditional_click_probs(self, session, labels):
        for rank, result in enumerate(session):
            if not labels[rank]:
                session[rank] = 1.0 - result
        return session

    def clicks_stat(self, mode=False):
        positions_of_clicks = np.zeros(10)
        clicks_per_sessions = np.zeros(11)
        for file_name in self.file_names:
            d = Data(self.data_dir, file_name, self.batch_size)
            for index in xrange(self.count_session / self.batch_size):
                print str(index) + '/' + str(self.count_session // self.batch_size)
                data, clicks = d.get_data_without_clicks()
                if mode is False:
                    generate_clicks = self.__get_clicks(data)
                else:
                    generate_clicks = clicks
                positions_of_clicks = np.sum([positions_of_clicks, np.sum([generate_clicks], axis=1)], axis=0)
                for pos in np.sum([generate_clicks], axis=2)[0]:
                    clicks_per_sessions[int(pos)] += 1
            positions_of_clicks /= np.sum([positions_of_clicks])
            positions_of_clicks = positions_of_clicks[0]
            clicks_per_sessions /= np.sum([clicks_per_sessions])
        return positions_of_clicks, clicks_per_sessions


def view_positions_of_clicks(clicks):
    x = np.arange(len(clicks[0][0]))
    bar_with = 0.15
    plt.bar(x, clicks[0][0], width=bar_with, color='green', zorder=2)
    plt.bar(x + bar_with, clicks[1][0], width=bar_with, color='red', zorder=2)
    plt.bar(x + bar_with * 2, clicks[2][0], width=bar_with, color='orange', zorder=2)
    plt.bar(x + bar_with * 3, clicks[3][0], width=bar_with, color='purple', zorder=2)

    plt.xticks(x + bar_with * 1.5, np.arange(1, 11))
    plt.xlabel('Позиции кликов'.decode('utf-8'))
    plt.ylabel('Проценты'.decode('utf-8'))

    green_patch = mpatches.Patch(color='green', label=clicks[0][1])
    red_patch = mpatches.Patch(color='red', label=clicks[1][1])
    orange_patch = mpatches.Patch(color='orange', label=clicks[2][1])
    purple_patch = mpatches.Patch(color='purple', label=clicks[3][1])

    plt.legend(handles=[green_patch, red_patch, orange_patch, purple_patch])
    plt.show()


def view_clicks_per_sessions(clicks):
    x = np.arange(len(clicks[0][0]))
    bar_with = 0.15
    plt.bar(x, clicks[0][0], width=bar_with, color='green', zorder=2)
    plt.bar(x + bar_with, clicks[1][0], width=bar_with, color='red', zorder=2)
    plt.bar(x + bar_with * 2, clicks[2][0], width=bar_with, color='orange', zorder=2)
    plt.bar(x + bar_with * 3, clicks[3][0], width=bar_with, color='purple', zorder=2)

    plt.xticks(x + bar_with * 1.5, np.arange(0, 11))
    plt.xlabel('Количество кликов'.decode('utf-8'))
    plt.ylabel('Проценты'.decode('utf-8'))

    green_patch = mpatches.Patch(color='green', label=clicks[0][1])
    red_patch = mpatches.Patch(color='red', label=clicks[1][1])
    orange_patch = mpatches.Patch(color='orange', label=clicks[2][1])
    purple_patch = mpatches.Patch(color='purple', label=clicks[3][1])

    plt.legend(handles=[green_patch, red_patch, orange_patch, purple_patch])
    plt.show()


if __name__ == '__main__':
    a = ['lstm', 'gan_stride']
    for i in a:
        framework = TestFramework(i,
                                  '/Users/vadim/pyclick2_nscm/pyclick/neural_models/code/',
                                  '/Users/vadim/pyclick2_nscm/pyclick/neural_models/code/',
                                  '/Users/vadim/',
                                  ['test_query_sessions_with_behavioral_features.part_10'],
                                  '/Users/vadim/pyclick2_nscm/pyclick/neural_models/code/',
                                  32,
                                  350000)
        framework.start_test()
        print framework.get_result()
