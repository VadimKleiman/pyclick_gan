import matplotlib.pyplot as plt

s = """batch 0 d_loss : 0.870849
batch 0 g_loss : 2.482948
batch 1 d_loss : 0.911860
batch 1 g_loss : 2.307631
batch 2 d_loss : 0.895134
batch 2 g_loss : 2.179356
batch 3 d_loss : 0.905461
batch 3 g_loss : 1.848192
batch 4 d_loss : 0.881197
batch 4 g_loss : 1.706760
batch 5 d_loss : 0.877756
batch 5 g_loss : 1.342278
batch 6 d_loss : 0.861354
batch 6 g_loss : 1.488061
batch 7 d_loss : 0.876647
batch 7 g_loss : 1.299643
batch 8 d_loss : 0.925502
batch 8 g_loss : 1.283709
batch 9 d_loss : 0.848233
batch 9 g_loss : 1.181855
batch 10 d_loss : 0.803443
batch 10 g_loss : 1.079778
batch 11 d_loss : 0.853775
batch 11 g_loss : 1.085909
batch 12 d_loss : 0.802046
batch 12 g_loss : 1.092261
batch 13 d_loss : 0.746507
batch 13 g_loss : 1.022701
batch 14 d_loss : 0.776392
batch 14 g_loss : 1.069228
batch 15 d_loss : 0.762577
batch 15 g_loss : 1.070803
batch 16 d_loss : 0.743093
batch 16 g_loss : 1.005988
batch 17 d_loss : 0.744528
batch 17 g_loss : 1.046207
batch 18 d_loss : 0.783775
batch 18 g_loss : 1.065853
batch 19 d_loss : 0.746786
batch 19 g_loss : 0.997020
batch 20 d_loss : 0.743127
batch 20 g_loss : 1.084180
batch 21 d_loss : 0.729788
batch 21 g_loss : 0.909759
batch 22 d_loss : 0.748885
batch 22 g_loss : 1.039116
batch 23 d_loss : 0.716225
batch 23 g_loss : 0.952388
batch 24 d_loss : 0.742134
batch 24 g_loss : 1.083411
batch 25 d_loss : 0.695199
batch 25 g_loss : 0.991697
batch 26 d_loss : 0.723115
batch 26 g_loss : 1.019698
batch 27 d_loss : 0.745086
batch 27 g_loss : 0.971928
batch 28 d_loss : 0.716955
batch 28 g_loss : 1.039951
batch 29 d_loss : 0.693497
batch 29 g_loss : 0.946812
batch 30 d_loss : 0.721829
batch 30 g_loss : 1.074856
batch 31 d_loss : 0.696641
batch 31 g_loss : 0.989103
batch 32 d_loss : 0.705380
batch 32 g_loss : 1.028844
batch 33 d_loss : 0.717791
batch 33 g_loss : 1.022121
batch 34 d_loss : 0.739686
batch 34 g_loss : 0.959960
batch 35 d_loss : 0.729595
batch 35 g_loss : 0.950266
batch 36 d_loss : 0.686181
batch 36 g_loss : 0.965524
batch 37 d_loss : 0.727437
batch 37 g_loss : 0.938843
batch 38 d_loss : 0.692983
batch 38 g_loss : 0.948001
batch 39 d_loss : 0.717008
batch 39 g_loss : 0.967811
batch 40 d_loss : 0.716587
batch 40 g_loss : 1.013011
batch 41 d_loss : 0.728016
batch 41 g_loss : 0.925316
batch 42 d_loss : 0.702868
batch 42 g_loss : 1.032193
batch 43 d_loss : 0.717279
batch 43 g_loss : 0.884806
batch 44 d_loss : 0.706535
batch 44 g_loss : 0.979646
batch 45 d_loss : 0.703155
batch 45 g_loss : 0.886220
batch 46 d_loss : 0.720101
batch 46 g_loss : 0.975911
batch 47 d_loss : 0.690590
batch 47 g_loss : 0.926891
batch 48 d_loss : 0.699977
batch 48 g_loss : 0.938790
batch 49 d_loss : 0.687940
batch 49 g_loss : 0.946460
batch 50 d_loss : 0.705415
batch 50 g_loss : 0.903556
batch 51 d_loss : 0.704871
batch 51 g_loss : 0.905815
batch 52 d_loss : 0.722632
batch 52 g_loss : 0.909726
batch 53 d_loss : 0.717514
batch 53 g_loss : 0.949962
batch 54 d_loss : 0.722841
batch 54 g_loss : 0.931960
batch 55 d_loss : 0.696591
batch 55 g_loss : 0.890298
batch 56 d_loss : 0.709221
batch 56 g_loss : 0.970817
batch 57 d_loss : 0.679908
batch 57 g_loss : 0.853031
batch 58 d_loss : 0.709745
batch 58 g_loss : 0.906822
batch 59 d_loss : 0.722287
batch 59 g_loss : 0.917029
batch 60 d_loss : 0.693592
batch 60 g_loss : 0.913177
batch 61 d_loss : 0.707658
batch 61 g_loss : 0.910358
batch 62 d_loss : 0.693534
batch 62 g_loss : 0.879773
batch 63 d_loss : 0.712814
batch 63 g_loss : 0.913620
batch 64 d_loss : 0.704439
batch 64 g_loss : 0.875182
batch 65 d_loss : 0.705753
batch 65 g_loss : 0.883712
batch 66 d_loss : 0.689669
batch 66 g_loss : 0.894720
batch 67 d_loss : 0.680962
batch 67 g_loss : 0.873676
batch 68 d_loss : 0.707964
batch 68 g_loss : 0.873758
batch 69 d_loss : 0.698083
batch 69 g_loss : 0.877104
batch 70 d_loss : 0.706661
batch 70 g_loss : 0.850749
batch 71 d_loss : 0.697899
batch 71 g_loss : 0.834705
batch 72 d_loss : 0.714953
batch 72 g_loss : 0.880445
batch 73 d_loss : 0.697850
batch 73 g_loss : 0.866876
batch 74 d_loss : 0.700878
batch 74 g_loss : 0.896364
batch 75 d_loss : 0.697631
batch 75 g_loss : 0.858865
batch 76 d_loss : 0.711261
batch 76 g_loss : 0.858533
batch 77 d_loss : 0.698849
batch 77 g_loss : 0.917965
batch 78 d_loss : 0.701651
batch 78 g_loss : 0.867302
batch 79 d_loss : 0.696603
batch 79 g_loss : 0.871470
batch 80 d_loss : 0.689769
batch 80 g_loss : 0.862066
batch 81 d_loss : 0.702191
batch 81 g_loss : 0.863455
batch 82 d_loss : 0.702410
batch 82 g_loss : 0.846814
batch 83 d_loss : 0.700591
batch 83 g_loss : 0.897264
batch 84 d_loss : 0.690850
batch 84 g_loss : 0.845608
batch 85 d_loss : 0.690752
batch 85 g_loss : 0.841780
batch 86 d_loss : 0.705739
batch 86 g_loss : 0.871269
batch 87 d_loss : 0.697787
batch 87 g_loss : 0.846575
batch 88 d_loss : 0.713297
batch 88 g_loss : 0.865016
batch 89 d_loss : 0.693919
batch 89 g_loss : 0.857253
batch 90 d_loss : 0.705880
batch 90 g_loss : 0.858711
batch 91 d_loss : 0.709419
batch 91 g_loss : 0.842387
batch 92 d_loss : 0.695540
batch 92 g_loss : 0.849650
batch 93 d_loss : 0.708258
batch 93 g_loss : 0.820185
batch 94 d_loss : 0.693281
batch 94 g_loss : 0.832330
batch 95 d_loss : 0.704019
batch 95 g_loss : 0.811548
batch 96 d_loss : 0.691822
batch 96 g_loss : 0.825932
batch 97 d_loss : 0.713407
batch 97 g_loss : 0.858452
batch 98 d_loss : 0.696224
batch 98 g_loss : 0.856442
batch 99 d_loss : 0.686467
batch 99 g_loss : 0.812248
batch 100 d_loss : 0.713288
batch 100 g_loss : 0.823331
batch 101 d_loss : 0.696501
batch 101 g_loss : 0.814771
batch 102 d_loss : 0.705243
batch 102 g_loss : 0.828819
batch 103 d_loss : 0.693368
batch 103 g_loss : 0.821924
batch 104 d_loss : 0.720445
batch 104 g_loss : 0.798389
batch 105 d_loss : 0.704142
batch 105 g_loss : 0.819747
batch 106 d_loss : 0.701251
batch 106 g_loss : 0.801396
batch 107 d_loss : 0.700309
batch 107 g_loss : 0.793338
batch 108 d_loss : 0.697196
batch 108 g_loss : 0.799678
batch 109 d_loss : 0.713049
batch 109 g_loss : 0.776616
batch 110 d_loss : 0.675640
batch 110 g_loss : 0.790759
batch 111 d_loss : 0.712111
batch 111 g_loss : 0.817590
batch 112 d_loss : 0.701318
batch 112 g_loss : 0.806517
batch 113 d_loss : 0.695905
batch 113 g_loss : 0.766393
batch 114 d_loss : 0.709314
batch 114 g_loss : 0.805407
batch 115 d_loss : 0.698545
batch 115 g_loss : 0.798556
batch 116 d_loss : 0.698236
batch 116 g_loss : 0.797546
batch 117 d_loss : 0.696114
batch 117 g_loss : 0.809260
batch 118 d_loss : 0.706706
batch 118 g_loss : 0.832283
batch 119 d_loss : 0.685424
batch 119 g_loss : 0.801346
batch 120 d_loss : 0.711197
batch 120 g_loss : 0.817817
batch 121 d_loss : 0.709507
batch 121 g_loss : 0.768940
batch 122 d_loss : 0.691015
batch 122 g_loss : 0.780396
batch 123 d_loss : 0.675314
batch 123 g_loss : 0.795614
batch 124 d_loss : 0.696678
batch 124 g_loss : 0.780765
batch 125 d_loss : 0.688837
batch 125 g_loss : 0.785851
batch 126 d_loss : 0.703023
batch 126 g_loss : 0.780740
batch 127 d_loss : 0.702748
batch 127 g_loss : 0.754598
batch 128 d_loss : 0.692544
batch 128 g_loss : 0.786570
batch 129 d_loss : 0.703580
batch 129 g_loss : 0.782420
batch 130 d_loss : 0.702483
batch 130 g_loss : 0.767910
batch 131 d_loss : 0.696241
batch 131 g_loss : 0.763339
batch 132 d_loss : 0.697973
batch 132 g_loss : 0.790928
batch 133 d_loss : 0.697913
batch 133 g_loss : 0.787588
batch 134 d_loss : 0.692545
batch 134 g_loss : 0.794749
batch 135 d_loss : 0.694739
batch 135 g_loss : 0.790246
batch 136 d_loss : 0.708044
batch 136 g_loss : 0.786450
batch 137 d_loss : 0.698371
batch 137 g_loss : 0.758456
batch 138 d_loss : 0.714457
batch 138 g_loss : 0.777180
batch 139 d_loss : 0.695795
batch 139 g_loss : 0.786256
batch 140 d_loss : 0.686001
batch 140 g_loss : 0.773734
batch 141 d_loss : 0.703371
batch 141 g_loss : 0.786039
batch 142 d_loss : 0.697509
batch 142 g_loss : 0.776195
batch 143 d_loss : 0.694262
batch 143 g_loss : 0.738397
batch 144 d_loss : 0.689413
batch 144 g_loss : 0.755592
batch 145 d_loss : 0.700456
batch 145 g_loss : 0.762440
batch 146 d_loss : 0.702871
batch 146 g_loss : 0.757383
batch 147 d_loss : 0.682884
batch 147 g_loss : 0.760172
batch 148 d_loss : 0.707913
batch 148 g_loss : 0.754690
batch 149 d_loss : 0.687598
batch 149 g_loss : 0.764992
batch 150 d_loss : 0.688791
batch 150 g_loss : 0.755515
batch 151 d_loss : 0.702321
batch 151 g_loss : 0.732635
batch 152 d_loss : 0.695305
batch 152 g_loss : 0.753959
batch 153 d_loss : 0.704250
batch 153 g_loss : 0.753209
batch 154 d_loss : 0.691798
batch 154 g_loss : 0.750911
batch 155 d_loss : 0.695226
batch 155 g_loss : 0.760905
batch 156 d_loss : 0.694222
batch 156 g_loss : 0.738398
batch 157 d_loss : 0.692357
batch 157 g_loss : 0.744730
batch 158 d_loss : 0.706243
batch 158 g_loss : 0.740299
batch 159 d_loss : 0.680223
batch 159 g_loss : 0.747968
batch 160 d_loss : 0.685809
batch 160 g_loss : 0.733911
batch 161 d_loss : 0.695281
batch 161 g_loss : 0.734422
batch 162 d_loss : 0.692154
batch 162 g_loss : 0.737748
batch 163 d_loss : 0.688751
batch 163 g_loss : 0.725967
batch 164 d_loss : 0.693119
batch 164 g_loss : 0.721923
batch 165 d_loss : 0.693837
batch 165 g_loss : 0.725202
batch 166 d_loss : 0.698325
batch 166 g_loss : 0.724222
batch 167 d_loss : 0.692169
batch 167 g_loss : 0.732665
batch 168 d_loss : 0.696218
batch 168 g_loss : 0.730154
batch 169 d_loss : 0.699375
batch 169 g_loss : 0.720920
batch 170 d_loss : 0.703513
batch 170 g_loss : 0.719971
batch 171 d_loss : 0.691557
batch 171 g_loss : 0.744326
batch 172 d_loss : 0.693862
batch 172 g_loss : 0.725932
batch 173 d_loss : 0.701260
batch 173 g_loss : 0.724698
batch 174 d_loss : 0.682690
batch 174 g_loss : 0.708338
batch 175 d_loss : 0.689191
batch 175 g_loss : 0.718110
batch 176 d_loss : 0.683865
batch 176 g_loss : 0.719329
batch 177 d_loss : 0.696925
batch 177 g_loss : 0.732693
batch 178 d_loss : 0.700515
batch 178 g_loss : 0.720607
batch 179 d_loss : 0.701075
batch 179 g_loss : 0.725057
batch 180 d_loss : 0.691280
batch 180 g_loss : 0.718218
batch 181 d_loss : 0.701773
batch 181 g_loss : 0.712809
batch 182 d_loss : 0.699696
batch 182 g_loss : 0.711043
batch 183 d_loss : 0.689125
batch 183 g_loss : 0.716431
batch 184 d_loss : 0.694282
batch 184 g_loss : 0.709238
batch 185 d_loss : 0.686277
batch 185 g_loss : 0.708850
batch 186 d_loss : 0.698210
batch 186 g_loss : 0.698585
batch 187 d_loss : 0.689378
batch 187 g_loss : 0.717990
batch 188 d_loss : 0.701159
batch 188 g_loss : 0.714675
batch 189 d_loss : 0.703951
batch 189 g_loss : 0.705865
batch 190 d_loss : 0.703048
batch 190 g_loss : 0.711883
batch 191 d_loss : 0.693953
batch 191 g_loss : 0.707755
batch 192 d_loss : 0.701800
batch 192 g_loss : 0.690571
batch 193 d_loss : 0.696560
batch 193 g_loss : 0.697008
batch 194 d_loss : 0.705400
batch 194 g_loss : 0.696984
batch 195 d_loss : 0.695560
batch 195 g_loss : 0.700003
batch 196 d_loss : 0.701621
batch 196 g_loss : 0.702850
batch 197 d_loss : 0.702768
batch 197 g_loss : 0.704858
batch 198 d_loss : 0.698447
batch 198 g_loss : 0.693687
batch 199 d_loss : 0.698567
batch 199 g_loss : 0.690163
batch 200 d_loss : 0.695218
batch 200 g_loss : 0.696040
batch 201 d_loss : 0.696758
batch 201 g_loss : 0.693549
batch 202 d_loss : 0.696267
batch 202 g_loss : 0.704373
batch 203 d_loss : 0.689994
batch 203 g_loss : 0.700657
batch 204 d_loss : 0.713693
batch 204 g_loss : 0.698819
batch 205 d_loss : 0.699773
batch 205 g_loss : 0.694327
batch 206 d_loss : 0.689600
batch 206 g_loss : 0.699534
batch 207 d_loss : 0.693115
batch 207 g_loss : 0.692161
batch 208 d_loss : 0.691403
batch 208 g_loss : 0.700026
batch 209 d_loss : 0.693551
batch 209 g_loss : 0.687452
batch 210 d_loss : 0.694624
batch 210 g_loss : 0.697847
batch 211 d_loss : 0.692043
batch 211 g_loss : 0.690568
batch 212 d_loss : 0.695977
batch 212 g_loss : 0.702244
batch 213 d_loss : 0.695432
batch 213 g_loss : 0.693693
batch 214 d_loss : 0.697330
batch 214 g_loss : 0.685971
batch 215 d_loss : 0.695249
batch 215 g_loss : 0.684333
batch 216 d_loss : 0.707264
batch 216 g_loss : 0.692092
batch 217 d_loss : 0.685610
batch 217 g_loss : 0.685073
batch 218 d_loss : 0.684833
batch 218 g_loss : 0.686830
batch 219 d_loss : 0.697851
batch 219 g_loss : 0.685783
batch 220 d_loss : 0.700925
batch 220 g_loss : 0.680508
batch 221 d_loss : 0.690897
batch 221 g_loss : 0.685168
batch 222 d_loss : 0.691849
batch 222 g_loss : 0.685204
batch 223 d_loss : 0.701610
batch 223 g_loss : 0.679290
batch 224 d_loss : 0.700777
batch 224 g_loss : 0.675783
batch 225 d_loss : 0.704867
batch 225 g_loss : 0.678512
batch 226 d_loss : 0.695964
batch 226 g_loss : 0.670437
batch 227 d_loss : 0.691348
batch 227 g_loss : 0.682309
batch 228 d_loss : 0.694946
batch 228 g_loss : 0.674651
batch 229 d_loss : 0.695835
batch 229 g_loss : 0.679488
batch 230 d_loss : 0.686159
batch 230 g_loss : 0.678841
batch 231 d_loss : 0.693091
batch 231 g_loss : 0.683182"""

def two_scales(ax1, time, data1, data2, c1, c2):
    """

    Parameters
    ----------
    ax : axis
        Axis to put two scales on

    time : array-like
        x-axis values for both datasets

    data1: array-like
        Data for left hand scale

    data2 : array-like
        Data for right hand scale

    c1 : color
        Color for line 1

    c2 : color
        Color for line 2

    Returns
    -------
    ax : axis
        Original axis
    ax2 : axis
        New twin axis
    """
    ax2 = ax1.twinx()

    ax1.plot(time, data1, color=c1, label='discriminator loss')
    ax1.set_xlabel('Iteration')
    ax1.set_ylabel('loss')
    ax1.legend(loc='upper left')

    ax2.plot(time, data2, color=c2, label='generator loss')
    ax2.set_ylabel('loss')
    ax2.legend(loc='upper right')
    return ax1, ax2

s = s.split('\n')
g = []
d = []
for n, i in enumerate(s):
    t = i.split(' ')
    if n % 2 == 0:
        d.append(float(t[4]))
    else:
        g.append(float(t[4]))
    # plt.figure(1)
    # plt.plot([0, 1], [0, 1], 'k--')
    # plt.plot(fpr_1, tpr_1, label='VC')
    # # plt.plot(fpr_2, tpr_2, label='BernoulliNB')
    # plt.xlabel('False positive rate')
    # plt.ylabel('True positive rate')
    # plt.title('ROC curve')
    # plt.legend(loc='best')
    # plt.show()


# plt.figure(1)
# # plt.plot(g, label='generator loss')
# plt.plot(d, label='discriminator loss')
# plt.xlabel('Iteration')
# plt.ylabel('loss')
# plt.legend(loc='best')
# print len(d)
# print len(g)
# plt.show()

_, ax = plt.subplots()
ax1, ax2 = two_scales(ax, [i for i in xrange(0, len(d))], d, g, 'r', 'b')


# Change color of each axis
def color_y_axis(ax, color):
    """Color your axes."""
    for t in ax.get_yticklabels():
        t.set_color(color)
    return None
color_y_axis(ax1, 'r')
color_y_axis(ax2, 'b')
plt.show()