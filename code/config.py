NUM_TRAIN_FILES = 70
PRINT_PROGRESS = True
FULL_TEST = True
PROGRESS_FREQUENCY = 100
MAX_TO_KEEP = 10 # Determines how many models are kept, when a new model is saved the oldest will be removed
RATIO = None # Percentage of sequential click sequences in batch (1.0 means all sequential, None means whatever is in the data)
MAX_CLICKSEQ_LEN = 3

TEST_SIZE = 100
BATCH_SIZE = 64
ITERATIONS = 1000000/BATCH_SIZE
#TEST_ITERATIONS = 20000/TEST_SIZE
TEST_ITERATIONS = 50000/TEST_SIZE
#TEST_ITERATIONS = 75
VALIDATION_ITERATIONS = int(50000/BATCH_SIZE)
OUTPUT_DIR = "output/TEST2/"
CHECKPOINT_DIR = "checkpoints/"
LOG_DIR = "logs/"
DATA_DIR = "/media/martijn/Data/Data/"
TEST_FILE = "test_query_sessions_with_behavioral_features.part_0.gz"
VALIDATION_FILE = "test_query_sessions_with_behavioral_features.part_1.gz"

LSTM_SIZE = 256
FINAL_LSTM_SIZE = 128 # Only for non-sequential model

ATTENTION_LSTM = 256
ATTENTION_FINAL_LSTM = 256
