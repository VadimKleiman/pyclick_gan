# coding=utf-8
import sys
import numpy as np

sys.path.insert(0, 'utils')
from batches import Batches
from config import *
import matplotlib.pyplot as plt
from pathlib import Path


class Statistic:
    def __init__(self, data_dir, files, batch_size=BATCH_SIZE, iterations=ITERATIONS):
        self.positions_of_clicks = np.zeros(10)
        self.clicks_per_sessions = np.zeros(11)
        self.iterations = iterations
        self.batch_size = batch_size
        self.data_dir = data_dir
        self.files = files
        self.error_files = 0

    def start(self):
        print 'DATA_DIR: ' + self.data_dir
        print 'FILES: ' + str(self.files)
        print 'BATCH_SIZE: ' + str(self.batch_size)
        print 'ITER: ' + str(self.iterations)
        for file_name in self.files:
            print 'Current file: test_query_sessions_with_behavioral_features.part_' + str(file_name)
            current_file = Path(self.data_dir + 'test_query_sessions_with_behavioral_features.part_' + str(file_name))
            if current_file.exists() and current_file.is_file():
                batches = Batches(self.data_dir + 'test_query_sessions_with_behavioral_features.part_' + str(file_name),
                                  self.batch_size)
                for i in xrange(0, self.iterations // self.batch_size):
                    print 'Current batch: ' + str(i) + '/' + str(self.iterations // self.batch_size)
                    _, _, _, _, click_pattern, _ = batches.create_batch()
                    self.positions_of_clicks = np.sum([self.positions_of_clicks, np.sum([click_pattern], axis=1)],
                                                      axis=0)
                    for pos in np.sum([click_pattern], axis=2)[0]:
                        self.clicks_per_sessions[int(pos)] += 1
            else:
                print 'ERROR: ' + self.data_dir + file_name
                self.error_files += 1
        self.positions_of_clicks /= np.sum([self.positions_of_clicks])
        self.positions_of_clicks = self.positions_of_clicks[0]
        self.clicks_per_sessions /= np.sum([self.clicks_per_sessions])
        return self.positions_of_clicks, self.clicks_per_sessions

    def get_stat(self):
        return self.positions_of_clicks, self.clicks_per_sessions


if __name__ == '__main__':
    st = Statistic('/Users/vadim/',
                   [3],
                   500,
                   1000000)
    poc, cps = st.start()
    x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    y = poc
    x_pos = np.arange(len(x))
    plt.bar(x_pos, y, align='center')
    plt.xticks(x_pos, x)
    plt.ylabel('Проценты'.decode('utf-8'))
    plt.xlabel('Количество кликов по позициям'.decode('utf-8'))
    plt.show()
    x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    y = cps
    x_pos = np.arange(len(x))
    plt.bar(x_pos, y, align='center')
    plt.xticks(x_pos, x)
    plt.ylabel('Проценты'.decode('utf-8'))
    plt.xlabel('Количество кликов за сессию'.decode('utf-8'))
    plt.show()
