import numpy as np
from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import BernoulliNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import ShuffleSplit
from sklearn.metrics import roc_auc_score
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_predict
from sklearn.ensemble import VotingClassifier
import cPickle
from data import *
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
from sklearn.decomposition import IncrementalPCA


class PartialClassifiers:
    def __init__(self):
        self.classifiers = {
            'MultinomialNB': MultinomialNB(),
            'BernoulliNB': BernoulliNB(),
            'Perceptron': Perceptron(max_iter=2000,
                                     tol=1e-3,
                                     n_jobs=-1,
                                     shuffle=True,
                                     random_state=42,
                                     penalty='elasticnet',
                                     alpha=1e-4),
            'SGD': SGDClassifier(n_jobs=-1,
                                 tol=1e-3,
                                 max_iter=2000,
                                 shuffle=True,
                                 random_state=42,
                                 penalty='elasticnet'),
            'PassiveAggressive': PassiveAggressiveClassifier(n_jobs=-1,
                                                             tol=1e-3,
                                                             max_iter=2000,
                                                             shuffle=True,
                                                             random_state=42)
        }

    def get_name_cls(self):
        return self.classifiers.keys()

    def partial_fit(self, X, y, cls_name='all'):
        if cls_name == 'all':
            for name, cls in self.classifiers.items():
                cls.partial_fit(X, y, classes=[0, 1])
        else:
            if cls_name in self.classifiers:
                self.classifiers[cls_name].partial_fit(X, y, classes=[0, 1])
            else:
                raise 'Classifier ' + cls_name + ' not found!'

    def predict(self, X, cls_name='all'):
        predict_array = []
        if cls_name == 'all':
            for name, cls in self.classifiers.items():
                predict_array.append(cls.predict(X))
            return predict_array
        if cls_name in self.classifiers:
            predict_array.append(self.classifiers[cls_name].predict(X))
            return predict_array
        raise 'Classifier ' + cls_name + ' not found!'

    def predict_proba(self, X):
        return [self.classifiers['MultinomialNB'].predict_proba(X), self.classifiers['BernoulliNB'].predict_proba(X)]

    def cross_val(self, X, y):
        scores = []
        for name, cls in self.classifiers.items():
            scores.append((name, cross_val_score(cls, X, y, n_jobs=-1, )))
        return scores

    def save(self, path_dir=None):
        if path_dir is None:
            path_dir = ''
        else:
            path_dir += '/'
        for name, cls in self.classifiers.items():
            with open(path_dir + name + '.pkl', 'wb') as fid:
                cPickle.dump(cls, fid)

    def load(self, path_dir=None):
        if path_dir is None:
            path_dir = ''
        for name in self.classifiers.keys():
            with open(path_dir + name + '.pkl', 'rb') as fid:
                self.classifiers[name] = cPickle.load(fid)


def unison_shuffled_copies(a, b):
    p = np.random.permutation(a.shape[0])
    return a[p], b[p]


if __name__ == '__main__':
    batch_size = 500
    d = Data('/Users/vadim/', 'test_query_sessions_with_behavioral_features.part_0', batch_size)
    ds = SimulateData('/Users/vadim/', 'test_query_sessions_with_behavioral_features.part_1', batch_size)
    c = PartialClassifiers()
    ipca = IncrementalPCA()
    # with open('ipca.pkl', 'rb') as fid:
    #     ipca = cPickle.load(fid)
    for i in xrange(0, 500):
        print i
        data, clicks = d.get_data_without_clicks()
        data_sim, clicks_sim = d.get_data_without_clicks()
        # data = ipca.transform(data)
        X_train = np.concatenate([data, clicks], axis=1)
        y_train = np.array([1] * batch_size)
        data_sim = ipca.transform(data_sim)
        X_train_sim = np.concatenate([data_sim, clicks_sim], axis=1)
        y_train_sim = np.array([0] * batch_size)
        X_all_train = np.concatenate([X_train, X_train_sim], axis=0)
        y_all_train = np.concatenate([y_train, y_train_sim], axis=0)
        X_all_train, y_all_train = unison_shuffled_copies(X_all_train, y_all_train)
        c.partial_fit(X_all_train, y_all_train)
    c.save('/Users/vadim/new_cls')
    predict = []
    target = []
    for i in xrange(0, 100):
        data, clicks = d.get_data_without_clicks()
        data_sim, clicks_sim = d.get_data_without_clicks()
        data = ipca.transform(data)
        X_train = np.concatenate([data, clicks], axis=1)
        y_train = np.array([1] * batch_size)
        data_sim = ipca.transform(data_sim)
        X_train_sim = np.concatenate([data_sim, clicks_sim], axis=1)
        y_train_sim = np.array([0] * batch_size)
        X_all_train = np.concatenate([X_train, X_train_sim], axis=0)
        y_all_train = np.concatenate([y_train, y_train_sim], axis=0)
        if i == 0:
            predict = c.predict(X_all_train)
            target = y_all_train
            continue
        tmp_predict = c.predict(X_all_train)
        for index in xrange(0, len(tmp_predict)):
            predict[index] = np.concatenate([predict[index], tmp_predict[index]])
        target = np.concatenate([target, y_all_train])

        # X_all_train, y_all_train = unison_shuffled_copies(X_all_train, y_all_train)
    # c.load()
    # vc = VotingClassifier(
    #         estimators=[
    #             ('MNB', c.classifiers['MultinomialNB']),
    #             ('BNB', c.classifiers['BernoulliNB']),
    #             ('SGD', c.classifiers['SGD']),
    #             ('P', c.classifiers['Perceptron']),
    #             ('PA', c.classifiers['PassiveAggressive'])],
    #         voting='soft')
    # predict = []
    # target = []
    # for i in xrange(0, 2):
    #     print i
    #     X_train, y_train = d.get_data()
    #     X_train_s, y_train_s = ds.get_data()
    #     X_all_train = np.concatenate([X_train, X_train_s])
    #     y_all_train = np.concatenate([y_train, y_train_s])
    #     if i == 0:
    #         predict = vc._predict_proba(X_all_train)
    #         target = y_all_train
    #         continue
    #     tmp_predict = vc._predict_proba(X_all_train)
    #     for index in xrange(0, len(tmp_predict)):
    #         predict[index] = np.concatenate([predict[index], tmp_predict[index]])
    #     target = np.concatenate([target, y_all_train])
    #
    # fpr_1, tpr_1, _ = roc_curve(target, predict[:, 1])
    # # fpr_2, tpr_2, _ = roc_curve(target, predict[:, 1])
    #
    # plt.figure(1)
    # plt.plot([0, 1], [0, 1], 'k--')
    # plt.plot(fpr_1, tpr_1, label='VC')
    # # plt.plot(fpr_2, tpr_2, label='BernoulliNB')
    # plt.xlabel('False positive rate')
    # plt.ylabel('True positive rate')
    # plt.title('ROC curve')
    # plt.legend(loc='best')
    # plt.show()
    # print roc_auc_score(target, predict[:, 1])
    # print roc_auc_score(target, predict[1][:, 1])


    # for index, name in enumerate(c.get_name_cls()):
    #     print name
    #     print accuracy_score(target, predict[index])
    # scores = c.cross_val(X_all_train, y_all_train)
    # for i in scores:
    #     print "Accuracy: %0.2f (+/- %0.2f)" % (i.mean(), i.std() * 2)

    # d = Data('/Users/vadim/', 'test.txt.gz', 10)
    # ds = SimulateData('/Users/vadim/', 'test.txt.gz', 10)
    # iter_count = 1000
    # c = PartialClassifiers()
    # while iter_count > 0:
    #     X_train, X_test, y_train, y_test = d.get_train_test()
    #     X_strain, X_stest, y_strain, y_stest = ds.get_train_test()
    #
    #     X_all_train = np.concatenate([X_train, X_strain])
    #     y_all_train = np.concatenate([y_train, y_strain])
    #     X_all_train, y_all_train = unison_shuffled_copies(X_all_train, y_all_train)
    #
    #     X_all_test = np.concatenate([X_test, X_stest])
    #     y_all_test = np.concatenate([y_test, y_stest])
    #     X_all_test, y_all_test = unison_shuffled_copies(X_all_test, y_all_test)
    #
    #     # ipca = IncrementalPCA(n_components=5, batch_size=14)
    #     # X_mini_train = ipca.fit_transform(X_all_train)
    #     # X_mini_test = ipca.fit_transform(X_all_test)
    #     X_all_train, y_all_train = unison_shuffled_copies(X_all_train, y_all_train)
    #     c.partial_fit(X_all_train, y_all_train)
    #     # c.predict(X_all_test, y_all_test)
    # c.save()
    # X_train, X_test, y_train, y_test = d.get_train_test()
    # X_strain, X_stest, y_strain, y_stest = ds.get_train_test()
    #
    # X_all_train = np.concatenate([X_train, X_strain])
    # y_all_train = np.concatenate([y_train, y_strain])
    # X_all_train, y_all_train = unison_shuffled_copies(X_all_train, y_all_train)
    #
    # X_all_test = np.concatenate([X_test, X_stest])
    # y_all_test = np.concatenate([y_test, y_stest])
    # X_all_test, y_all_test = unison_shuffled_copies(X_all_test, y_all_test)
    # X_all_train, y_all_train = unison_shuffled_copies(X_all_train, y_all_train)
    # c.predict(X_all_train, y_all_train)
