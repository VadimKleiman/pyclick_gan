import numpy as np
from sklearn.decomposition import IncrementalPCA
from data import Data
import cPickle

if __name__ == '__main__':
    batch_size = 500
    d1 = Data('/Users/vadim/', 'test_query_sessions_with_behavioral_features.part_0', batch_size)
    d2 = Data('/Users/vadim/', 'test_query_sessions_with_behavioral_features.part_1', batch_size)
    ipca = IncrementalPCA(n_components=100, batch_size=100)
    for i in xrange(0, 300000 / batch_size):
        print i
        data = d1.get_data_without_clicks()
        ipca.partial_fit(data)
    for i in xrange(0, 300000 / batch_size):
        print i
        data = d2.get_data_without_clicks()
        ipca.partial_fit(data)
    with open('ipca.pkl', 'wb') as fid:
        cPickle.dump(ipca, fid)